## Terms

- **to mint a DOI**: to register a DOI; 
- **ARK (Archival Resource Key)**: alternative to DOI; applicable to libraries, museums, archives; 
- **ANDS (Australian National Data Service)**: state-funded org for research data; has a service to issue DOIs; 
- **EZID (EaZy ID)**: main DOI supplier for figshare; from University of California; mostly for US Universities; horrible name; 
- **DataCite**: DOI supplier organization; various member institutions, mostly European (e.g. CERN); 
- **Identifier resolution**: redirection from DOI URL to the actual resource URL; 
- **DOI prefix**: the id of the organization that wants to register DOIs; 
- **DOI shoulder**: ID for a random namespace under the organization DOI prefix that has the role of avoiding collisions; 
- **SPT (suffix pass-through)**: any suffix added to the ARK will be copied to the redirected location URL; works only with ARK; 

