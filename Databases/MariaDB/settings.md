## Settings

- **skip-external-locking**: do not use an external lock file to control access to the database; this file is used by MyISAM to solve the problem of 2 or more processes which want to gain access to the same database files, which makes it useless if there is a single process on the machine which does this; 
- **log_bin_trust_function_creators**: enables non-SUPER users to create functions; also enables function creation syntax; 
- **sort_buffer_size**: ammount of memory allocated to every sort that is queried on the server; 
- **read_rnd_buffer_size**: ammount of buffer memory allocated to reading rows from a MyISAM table after a key sort; 
- **bulk_insert_buffer_size**: ammount of buffer memory allocated to the cache tree used in bulk inserts; 
- **max_connect_errors**: after this many consecutive connection errors the client host is blocked from connecting ever again to the database; the blocked hosts cache is flushed with FLUSH HOSTS; 
- **myisam_recover**: what strategy to choose when a failed transaction is detected and the engine decides to recover the table; if BACKUP, it will save a backup database file with the current datetime stamp in name; 
- **key_buffer_size**: size of the buffer for index blocks used by MyISAM tables; 25% or more of the available server memory is recommended; 
- **concurrent_insert**: 3 possbile values – 0, for no concurrent inserts allowed; 1 for concurrent inserts allowed in tables with no free blocks of data; 2 for all tables, if there are concurrent requests, the new rows are appended at the end; 
- **table_open_cache**: number of open tables for all threads; 
- **tmp_table_size**: maximum mermory allowed for temporary tables; often GROUP BY queries are responsible for exceeding this limit; if it is exceeded, it will be transformed to a MyISAM or Aria table; 
- **open_files_limit**: max number of file descriptors that can be opened by the server; 

