
### Vagrant plugin

- `install_master` - should vagrant install the salt-master on the machine 
- `run_highstate` - executes `state.highstate` on vagrant up; 


### Terms 

- Top file – file (`top.sls`) containing a mapping between: 
  - Group(s) of machines 
  - Configuration roles 
- Configuration role – a group of attributes that a machine must have and which is known under a specific name;
  e.g. the `apache` configuration role could denote that the apache server must run on the machine and the corresponding `sls` file is called `apache.sls`; 
- Environment – a directory containing a top file and a set of state files; 
- Grains – info about the underlying system; grains of data can also be added manually in the minion config file or in the `/etc/salt/grains` file on the minion; 
- Pillar – collection of data that can be distributed to minions; the data is accessible only by the minion for which it is targeted in the pillar configuration; 
- `file_roots` - the base directory for files that are associated with environments and which can then be accessible by minions through the master daemon; a base environment is required to house the top file; kept in 0mq; 
- `pillar_roots` - base directories for environments, same as `file_roots`, but for pillar; there must be a base which contains the top file which is used in filtering access to data based on minion roles and names; 

