## Language features
 
### Internal plumbing 

#### Tuple

- Internally it is simply an array of pointers
- PyTuple_MAXSAVESIZE: tuples of this size or smaller can be reclaimed after
they are declared dead; if you ever need to allocate a tuple of size n and one
has previously been allocated and is no longer in use, CPython can just recycle
the old array;
- The empty tuple is a singleton and it always exists in the interpreter memory
space;

#### List
- Array of pointers to PyObject*;
- The array is enlarged every time the list is in danger of being overcrowded by
(newsize >> 3) + (newsize < 9 ? 3 : 6);

#### Set
- A dictionary without keys;

#### Dictionary
- [PyCon 2010: The Mighty Dictionary](http://www.laurentluce.com/posts/python-dictionary-implementation/)

