## Web vulnerabilities

- **Cross-Site-Scripting - XSS**
  - an attacker can execute something malicious in the victim's browser by
    crafting a URL which contains bits of data that cause the site to execute
    alien code;
  - the data provided by a web client, most commonly in HTTP query parameters
    (e.g. HTML form submission), is used immediately by server-side scripts to
    parse and display a page of results for and to that user, without properly
    sanitizing the request and/or response;

- **Persistent XSS** 
  - the data provided by the attacker is saved by the server, and then
    permanently displayed on "normal" pages returned to other users in the
    course of regular browsing, without proper HTML escaping;

- **SQL injection** 

- **Remote code execution**
  - Attacker makes a request that causes the server to execute malicious code; 
  - Old attack, generally in the PHP ecosystem; 

- **Cross-Site Request Forgery - CSRF** 
  - a malicious web site, email, blog, instant message, or program causes a
    user’s web browser to perform an unwanted action on a trusted site for which
    the user is currently authenticated;

- **Insecure Direct Object References**
  - an internal object such as a file or database key is exposed to the user

- **Configuration vulnerabilities**
  - DEBUG in production (stack traces appear to the user in case of error)

- **Invalidated redirects and forwards**
  - redirection to a site specified in the GET request which is not filtered /
    validated;
  - Leads to potential malicious URL forgeries;

- **Username enumeration**
  - the backend validation script tells the attacker if the supplied username is
    correct or not

- **HTTP response splitting**:
  - The attacker makes the server print a carriage return (CR, ASCII 0x0D) line
    feed (LF, ASCII 0x0A) sequence followed by content supplied by the attacker
    in the header section of its response, typically by including them in input
    fields sent to the application;
  - The generic solution is to URL-encode strings before inclusion into HTTP
    headers such as Location or Set-Cookie;

- **Session fixation**
  - The possibility of an attacker to set/find a victim's session ID;
  - mostly relies on session identifiers being accepted from URLs (query string)
    or POST data;

- **Session hijacking**

