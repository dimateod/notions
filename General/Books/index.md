## Diverse

- Programming Pearls - Jon Bentley 
- Foundations of Computer Science - Al Aho and Jeff Ullman
- Code Complete: A Practical Handbook of Software Construction, Second Edition - Steve McConnell
- Modern Operating Systems - Andrew Tanenbaum
 

## Algorithms

- Introduction to Algorithms - Cormen et al. 
- Data Structures and Algorithmic Thinking with Python - Narasimha Karumanchi 


## Networking 

- Computer Networks - Tanenbaum & Wetherall 


## Design patterns
- [https://sourcemaking.com/design_patterns](https://sourcemaking.com/design_patterns)


