## OOP Model

- __Abstraction__: The process of picking out (abstracting) common features of objects and procedures;
- __Encapsulation__: The process of combining elements to create a new entity. A procedure is a type of encapsulation because it combines a series of computer instructions;
- __Polymorphism__: A programming language's ability to process objects differently depending on their data type or class;
- __Inheritance__: a feature that represents the "is a" relationship between different classes;
 
- __Class__: A category of objects. The class defines all the common properties of the different objects that belong to it;
- __Object__: a self-contained entity that consists of both data and procedures to manipulate the data;
 
- __Information hiding__: The process of hiding details of an object or function. Information hiding is a powerful programming technique because it reduces complexity;
- __Interface__: the languages and codes that the applications use to communicate with each other and with the hardware;
- __Messaging__: Message passing is a form of communication used in parallel programming and object-oriented programming;
- __Procedure__: a section of a program that performs a specific task;
 
- __Association__: a relationship where all objects have their own lifecycle and there is no owner, but they can be associated with each other;
- __Aggregation__: a specialized form of Association where all objects have their own lifecycle, but there is ownership and child objects can not belong to another parent object;
- __Composition__: a specialized form of Aggregation and we can call this as a “death” relationship. It is a strong type of Aggregation. Child object does not have its lifecycle and if parent object is deleted, all child objects will also be deleted;

