## SOLID 
 
- __S – Single-responsibility principle__: A class should have one and only one reason to change, meaning that a class should have only one job. 
- __O – Open-closed principle__: Objects or entities should be open for extension, but closed for modification. 
- __L – Liskov substitution principle__: Let q(x) be a property provable about objects of x of type T. Then q(y) should be provable for objects y of type S where S is a subtype of T. 
- __I – Interface segregation principle__: A client should never be forced to implement an interface that it doesn’t use or clients shouldn’t be forced to depend on methods they do not use. 
- __D – Dependency Inversion Principle__: Entities must depend on abstractions not on concretions. 

